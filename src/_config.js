export const process = {
	dev: true
}

export const site = {
	home : process.dev ? 'http://localhost:8080/' : 'https://tocode.ru/',
	app : process.dev ? 'http://app.localhost:8080/' : 'https://app.tocode.ru/',
}

export const links = [
	{
		titel: 'Home',
		alias: 'home',
		url: '/',
	},
	{
		titel: 'About',
		alias: 'about',
		url: '/about',
	}
]

export const app = {
	title: 'Template'
}