import {createRouter, createWebHashHistory} from "vue-router"

const routerHistory = createWebHashHistory()

import HomePage from "./pages/homePage"
import AboutPage from "./pages/aboutPage"
import NoteFound from "./pages/noteFound"
// const HomePage = () => import("./pages/homePage")
// const AboutPage = () => import("./pages/aboutPage")


const routers = createRouter({
	history: routerHistory,
	routes: [
		{
			path: "/",
			name: "home",
			component: HomePage,
		},
		{
			path: "/about",
			name: "about",
			component: AboutPage,
		},
		/*{
			path: "/:PathMatch(.*)*",
			name: "404",
			component: NoteFound,
		},*/
		{
			path: "/:CatchAll(.*)",
			name: "404",
			component: NoteFound,
		},
	],
})

export default routers